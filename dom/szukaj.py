# -*- encoding: utf-8 -*-
__author__ = 'Joanna667'
import getpass
import re
import requests
import operator
import httplib2
from bs4 import BeautifulSoup

co = raw_input('Podaj czego szukasz: ')
gdzie = raw_input('Podaj swoje miasto: ')
klucz = raw_input('Podaj słowo kluczowe: ')
mojAdres = raw_input('Podaj swój adres (np. Nowoursynowska 159, Warszawa): ')
print "\n"
#wzór adresu:
# Jontka 8, Warszawa

def usunOgonki(ciag):
    trans = dict((ord(a), b) for a, b in zip(u'ąćęłńóśżźĄĆĘŁŃÓŚŻŹ', u'acelnoszzACELNOSZZ'))
    return ciag.translate(trans)

def wytyczTrase(poczatek, koniec):
    pocz = poczatek.replace(" ", "+")
    kon = koniec.replace(" ", "+")

    sensor = 'false'
    language = 'pl'
    strona = 'http://maps.googleapis.com/maps/api/directions/json?origin='+pocz+'&destination='+kon+'&sensor='+sensor+'&language='+language

    strona = usunOgonki(strona)
    session = requests.session()
    odp = session.get(strona)
    slownik = odp.json()
    try:
        odl = slownik["routes"][0]["legs"][0]["distance"]["text"]
        odpowiedz = "\n\nOdleglosc wynosi: "+odl+".\nAby dojechac na miejsce: \n"
        steps = slownik["routes"][0]["legs"][0]["steps"]
        for cos in steps:
            a = cos["distance"]["text"]
            komenda = cos["html_instructions"]
            komenda = re.sub('<[^<]+?>', '', komenda)
            odpowiedz += a+" "+komenda+"\n"
        return odpowiedz
    except:
        print "Nie udało się wytyczyć trasy"
#PRZYKŁADOWE DANE:

# co = 'centrum handlowe'
# gdzie = 'warszawa'
# klucz = 'galeria'
strona = 'http://www.pkt.pl/'+co+'/'+gdzie+'/3-1/?oWhat='+klucz+'&Where='+gdzie
session = requests.session()
odp = session.get(strona)
lokalizacja = odp.text
miejsce = BeautifulSoup(lokalizacja)
punkty = {"": ""}
adresy = miejsce.findAll('h2')
if len(adresy) == 0:
    print "Nic nie znaleziono :("
else:
    for cos in adresy:
        nazwa = cos.findNext().text
        adres = cos.findNext().findNext().text
        if adres[0].isdigit():
            adres = adres[6:]
        punkty.update({nazwa: adres})

    i = 0
    del punkty[""]
    for nazwa in punkty:
        print str(i)+": "+nazwa + ", " + punkty[nazwa]
        i += 1

    print("Wytyczanie trasy: ")
    idd = raw_input('\nWpisz numer interesującej cię pozycji: ')
    ctn = False
    while not ctn:
        try:
            idd = int(idd)
            if idd in range(0,i):
                ctn = True
            else:
                idd = raw_input('Podano zły zakres: ')
        except:
            idd = raw_input('Należy podać liczbę, wpisz ponownie: ')
    i = 0

    for cos in punkty:
        if idd == i:
            print wytyczTrase(mojAdres, punkty[cos])
        i += 1


