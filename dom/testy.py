# -*- encoding: utf-8 -*-
from szukaj import *
__author__ = 'Joanna667'

import unittest


class MyTestCase(unittest.TestCase):
    def parsing_string(self):
        lancuch = "Zażółć gęślą jaźń"
        template = "Zazolc gesla jazn"
        lancuch2 = usunOgonki(lancuch)
        self.assertTrue(template==lancuch2)

    def route_test(self):
        poczatek = "Jontka 8, Warszawa"
        koniec = "Nowoursynowska 159, Warszawa"
        komunikat = "Nie udało się wytyczyć trasy"
        odp = wytyczTrase(poczatek, koniec)
        self.assertFalse(komunikat==odp)



if __name__ == '__main__':
    unittest.main()
